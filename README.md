# Stem Cell Karyotypes

Analysis of 15,000 stem cell karyotypes from WiCell. Material in this
project is designed to support the submitted manuscript:

McIntire EM, Abrams ZB, Leonhard KA, Coombes KR, Abruzzo LV, Taapken SM.
_A Ten-Year Retrospective Analysis of Human Pluripotent Stem Cell Karyotyping._
Submitted to Cell Stem Cell.

# Project Layout
The folder organization used here is based on publications
recommending separation of code, data, results, and scratch into four
distinct units/locations.

All code is written in the Rmarkdown format using the R Statistical
Programming Environment.

Data is divided into "`raw`" (as received directly from investigators
or other software such as the `cytogps.org` website maintained by our
collaborators at Washington University in St. Louis) or "`clean`"
(meaning it has been processed by our own code into a nicer format).
Intermediate results are typcally stored in binary R data format in
the `scratch` directory.

The "`results`" folder contains figures (usually in PNG format) and
spreadsheets (usually created from R in comma-separated-files formet
and then manually converted into Excel worksheets.

The four units can be stored at different relative or absolute
locations on different computers by creating a file in JavaScript
Object Notation (JSON) called `wicell.json` and storing that file in the
directory `$HOME\Paths` on the local machine. The Git repository
contains a sample version of this file that can be edited to satisfy
local constraints.

