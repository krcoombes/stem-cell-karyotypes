---
author: Kevin R, Coombes
date:  21 July 2019
title: CytoGPS/data/WiCell
---

# Overview
This folder contains both raw and cleaned data related to the WiCell
karyotypes derived from embryonic and induced pluripotent stem cells.
