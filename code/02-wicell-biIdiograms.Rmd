---
author: "Kevin R. Coombes"
title:  "WiCell Two-sided Idiograms"
date:   "11 September 2019"
output:
  html_document:
    toc: true
    highlight: kate
    theme: readable
---

```{r setup, include=FALSE, results="hide"}
knitr::opts_chunk$set(echo = TRUE)
options(width = 80)
```
```{r mycss, results="asis", echo=FALSE}
cat('
<style type="text/css">
b, strong {color: red; }
i, em {color: blue; }
.defn {color: purple; }
.para {color: purple;
      font-weight: bold;
}
.figure { text-align: center; }
.caption { font-weight: bold; }
</style>
')
```

# Overview
The goal of this analysis is display the summary
statistics fromour analysis of the WiCell karyotype data.

# Getting Started
First, we load the generic idiogram data produced in the previous report.
```{r idio}
source("00-paths.R")
load(file.path(paths$clean, "idiogram.Rda"))
```

# WiCell Data
Now we read the stem cell frequency results. These are stored (sigh) 
in an Excel spreadsheet.
```{r wisc}
suppressMessages( suppressWarnings( library(gdata) ))
wisc <- read.csv(file.path(paths$clean, "WiCell_ESC_IPSC.csv"), row.names=1)
wisc <- as.data.frame(100*as.matrix(wisc))
dim(wisc)
summary(wisc)
```

We are now going to go through some contortions to split the band-rownames
into meaningful pieces.
```{r wiscAnnotate}
foo <- strsplit(rownames(wisc), "[pq]")
chr <- sapply(foo, function(x) paste("chr", x[1], sep=''))
wisc$Chromosome <- factor(chr, 
                          levels <- paste("chr", c(1:22, "X", "Y"), sep=""))
nc <- sapply(foo, function(x) nchar(x[1]))
table(nc)
arm <- substring(rownames(wisc), nc+1, nc+1)
table(arm)
wisc$Arm <- factor(arm)
band <- paste(arm, pt <- sapply(foo, function(x) x[2]), sep='')
pp <- paste("p", rev(sort(unique(pt[arm=="p"]))), sep="")
qq <- paste("q", sort(unique(pt[arm=="q"])), sep="")
B <- factor(band, levels=c(pp, qq))
wisc$Band <- B
```

In our (unshown) comparison between idiograms and CytoGPS features, we found that
one of the segments in the WiCell data has an extra space in the name. We remove
that space.
```{r despace}
w <- which(rownames(wisc) == "19q13.2 ")
rownames(wisc)[w] <- "19q13.2"
rm(w)
head(wisc)
summary(wisc)
```

# Compare Idiograms and CytoGPS
The row names of both the <tt>idio</tt> and <tt>wisc</tt> objects are supposed to
be the ISCN names of cytobands. Here we compare those names.
```{r comp}
all(rownames(idio) %in% rownames(wisc)) # finally
R <- rownames(wisc)[!(rownames(wisc) %in% rownames(idio))]
R
wisc <- wisc[rownames(idio),]
```
The "only" difference is that the CytoGPS-produced <tt>wisc</tt> data contains extra
entries. These are not part of the standard; they were "invented" by Zach Abrams to
solve some technical problems. We (eventually) plan to just throw them away.

# Idiograms
We define a palette to mark the tpes of stem cells
```{r, fig.cap="LGF palette"}
library(Polychrome)
data(colorsafe)
pal <- colorsafe[c(1, 10)]
pal <- colorsafe[c(9, 8)]
names(pal) <- c("ESC", "IPSC")
swatch(pal)
```


## Bi-idiogram
Here we define a function to make an idiogram that shows different data on either side of each
chromosome.
```{r biIdiogram}
# DATA must have three columns, one of shich is "Chromosome" containing
# entries of the form "chr1".
biIdiogram  <- function(DATA, leftcol, rightcol, 
                        pal = c("blue", "red"), 
                        horizontal=FALSE, nrows = 2) {
  if(!nrows %in% 1:4) {
    stop("Number of rows must be 1, 2, 3, or 4.")
  }
  opar <- par(bg="white")
  on.exit(par(opar))
  # vertical layout, two rows
  L1 <- function() {
    layout(matrix(1:72, 1, 72, byrow = TRUE),
           heights = c(1), widths=rep(c(1.1, 0.8, 1.1), times = 24))
  }
  L2 <- function() {
    layout(matrix(1:72, 2, 36, byrow = TRUE),
           heights = c(1,1), widths=rep(c(1.1, 0.8, 1.1), times = 12))
  }
  L3 <- function() {
    layout(matrix(1:72, 3, 24, byrow = TRUE),
           heights = c(1,1, 1), widths=rep(c(1.1, 0.8, 1.1), times = 8))
  }
  L4 <- function() {
    layout(matrix(1:72, 4, 18, byrow = TRUE),
           heights = c(1,1, 1, 1), widths=rep(c(1.1, 0.8, 1.1), times = 6))
  }
  switch(nrows, L1(), L2(), L3(), L4())

  for (I in c(1:22, "X", "Y")) { # for each chromosome
    chrname <- paste('chr', I, sep='')
    dumbposn <- seq(1, 250000000, length=2500)
    clap <- idio[idio$Chromosome == chrname,]
    segset <- DATA[DATA$Chromosome == chrname,]
    leftvals <- rightvals <- y <- rep(NA, length(dumbposn))
    for(J in 1:nrow(clap)) {
      y[clap[J, "loc.start"] <= dumbposn & 
          dumbposn <= clap[J, "loc.end"] ] <- as.numeric(clap[J, "Stain"])
      rightvals[clap[J, "loc.start"] <= dumbposn & 
          dumbposn <= clap[J, "loc.end"] ] <- as.numeric(segset[J, rightcol])
      leftvals[clap[J, "loc.start"] <= dumbposn & 
          dumbposn <= clap[J, "loc.end"] ] <- as.numeric(segset[J, leftcol])
    }
    resn <- max(max(segset[,leftcol]), max(segset[,rightcol]))
    ## left bars
    par(mai=c(0, 0.001, 0.5, 0.001))
    barplot(-rev(leftvals), horiz=T, border=NA, col=pal[1],
            xlim=c(-1.05*resn, 0), yaxs="i", xaxt="n", space=0)
    ## chromosomes
    par(mai=c(0, 0.02, 0.5, 0.02))
    image(1:1, dumbposn, matrix(rev(y), nrow=1), col=idiocolors, bty='n',
          xlab='', ylab='', xaxt='n', yaxt='n', zlim=c(1, 8),
          main=paste("Chr", I), cex=0.8)
    pts <- max(dumbposn) - c(min(clap$loc.start), max(clap$loc.end))
    abline(h=pts)
    lines(c(1.4, 1.4), pts)
    lines(c(0.6, 0.6), pts)
    ## right bars
    par(mai=c(0, 0.001, 0.5, 0.001))
    barplot(rev(rightvals), horiz=T, border=NA, col=pal[2],
            xlim=c(0, 1.05*resn), yaxs="i", xaxt="n", space=0)
  }
  layout(1,1,1)
  par(new = TRUE)
  legend(0.3*par("usr")[2], 0.15*par("usr")[4], c(leftcol, rightcol), col=pal, pch=15)
#  box()
  invisible(DATA)
}

```

```{r}
res <- 300
png(file = file.path(paths$results, "idio-losses-2.png"),
    width=18*res, height=10*res, res=res, bg="white")
biIdiogram(wisc, "ESC.Loss", "IPSC.Loss", pal)
dev.off()
```

![Losses by Chromosome](`r file.path(paths$results, "idio-losses-2.png")`)

```{r}
png(file = file.path(paths$results, "idio-gains-2.png"),
    width=18*res, height=10*res, res=res, bg="white")
biIdiogram(wisc, "ESC.Gain", "IPSC.Gain", pal)
par(mai=c(1,1,1,1), usr=c(0,1,0,1))
text(0.5, 0.5, "Misleading", col="red", cex=10, srt=30)
dev.off()
```

![Gains by Chromosome](`r file.path(paths$results, "idio-gains-2.png")`)

```{r}
png(file = file.path(paths$results, "idio-fusions-2.png"),
    width=18*res, height=10*res, res=res, bg="white")
biIdiogram(wisc, "ESC.Fusion", "IPSC.Fusion", pal)
dev.off()
```

![Fusions by Chromosome](`r file.path(paths$results, "idio-fusions-2.png")`)

## Testing

```{r onerow}
png(file = file.path(paths$results, "idio-gains-1.png"),
    width=36*res, height=8*res, res=res, bg="white")
biIdiogram(wisc, "ESC.Gain", "IPSC.Gain", pal, nrow=1)
dev.off()
png(file = file.path(paths$results, "idio-losses-1.png"),
    width=36*res, height=8*res, res=res, bg="white")
biIdiogram(wisc, "ESC.Loss", "IPSC.Loss", pal, nrow=1)
dev.off()
png(file = file.path(paths$results, "idio-fusions-1.png"),
    width=36*res, height=8*res, res=res, bg="white")
biIdiogram(wisc, "ESC.Fusion", "IPSC.Fusion", pal, nrow=1)
dev.off()
```


![Gains by Chromosome](`r file.path(paths$results, "idio-gains-1.png")`)

```{r threerows}
png(file = file.path(paths$results, "idio-gains-3.png"),
    width=12*res, height=12*res, res=res, bg="white")
biIdiogram(wisc, "ESC.Gain", "IPSC.Gain", pal, nrow=3)
dev.off()
png(file = file.path(paths$results, "idio-losses-3.png"),
    width=12*res, height=12*res, res=res, bg="white")
biIdiogram(wisc, "ESC.Loss", "IPSC.Loss", pal, nrow=3)
dev.off()
png(file = file.path(paths$results, "idio-fusions-3.png"),
    width=12*res, height=12*res, res=res, bg="white")
biIdiogram(wisc, "ESC.Fusion", "IPSC.Fusion", pal, nrow=3)
dev.off()
```

![Gains by Chromosome](`r file.path(paths$results, "idio-gains-3.png")`)

```{r fourrows}
png(file = file.path(paths$results, "idio-gains-4.png"),
    width=10*res, height=16*res, res=res, bg="white")
biIdiogram(wisc, "ESC.Gain", "IPSC.Gain", pal, nrow=4)
dev.off()
png(file = file.path(paths$results, "idio-losses-4.png"),
    width=10*res, height=16*res, res=res, bg="white")
biIdiogram(wisc, "ESC.Loss", "IPSC.Loss", pal, nrow=4)
dev.off()
png(file = file.path(paths$results, "idio-fusions-4.png"),
    width=10*res, height=16*res, res=res, bg="white")
biIdiogram(wisc, "ESC.Fusion", "IPSC.Fusion", pal, nrow=4)
dev.off()
```

![Gains by Chromosome](`r file.path(paths$results, "idio-gains-4.png")`)

# Appendix
These computations were performed in the following environment:
```{r si}
sessionInfo()
```
and in the following directory:
```{r where}
getwd()
```
