---
title: 'WiCell 201: The Number of the Counting'
author: "Kevin R. Coombes"
date: "`r Sys.Date()`"
output:
  html_document:
    highlight: kate
    theme: readable
    toc: yes
  pdf_document:
    toc: yes
---

```{r setup, include=FALSE, results="hide"}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_chunk$set(fig.path = "../results/figs/")
options(width = 80)
```
```{r mycss, results="asis", echo=FALSE}
cat('
<style type="text/css">
b, strong {color: red; }
i, em {color: blue; }
.defn {color: purple; }
.para {color: purple;
      font-weight: bold;
}
.figure { text-align: center; }
.caption { font-weight: bold; }
</style>
')
```

# Executive Summary
## Background
In the analyses to date, frequencies were produced automatically by the CytoGPS algorithm
at the website http://cytogps.org. Manual processing before using CytoGPS consisted of
removing obviously "completely normal" karyotypes, and only sending the remaining karyotypes
through CytoGPS. The CytoGPS algorithm splits each karyotype into separate clones, and
computes frequencies based on the resulting number of clones. In the stem cell data, many
karyotypes include a normal clone as well as one or more abnormal clones. *Thus, the frequencies
use a denominator best described as "the number of clones contained in cultures that
contain at least one abnormal clone."* In addition to being convoluted and hard to describe
to readers, that denominator is probably the wrong thing to use.

The goal of this analysis is to understand how to correctly compute frequencies
by carefully determining the appropriate denominators. This goal requires us to
fully undertand how the data have been processed so we can count everything correctly.
 
A second goal it to determine if multiple abnormal clones arising in the same culture
are more likely to represent clonal evolution or clonal divergence.

## Methods
We carefully reviewed the data through various steps in its processing. At multiple
points along the process, we computed $2\times2$ contingency tables breaking down the
counts by type of stem cell and by whether or not entities were normal or abnormal. We
applied chi-squared tests to these tables to test for independence.

## Results
```{r summarizeCounts, echo=FALSE, results = "hide"}
summarizeCounts <- function(M) {
  R <- apply(M, 1, sum)
  M <- cbind(M, Total = R)
  C <- apply(M, 2, sum)
  M <- rbind(M, Total = C)
  perc <- round(100 * M["Abnormal",]/C, 1)
  chi <- chisq.test(M[1:2, 1:2])
  list(Counts = M, "PercentAbnormal" = perc, Chi.Squared = chi)
}
```
```{r echo=FALSE, results = "hide"}
source("00-paths.R")
load(file.path(paths$results, "counters.rda"))
```
Here are the main results. First, we count the number of cultures, broken down by
type of stem cell and by whether or not they contain an abnormality.
```{r}
summarizeCounts(countByCulture)
```
Second, we we count the number of clones.
```{r}
summarizeCounts(countByClone)
```

Finally, we can show that many of the multiple clones are independent, in the sense that they
share no abnormaliites in common. Even where there are shared abnormaliities, a more detailed look 
suggests that they may often be independent (the shared abnormality being the fusion-containing
cytoband from fusions involving different partner chromosomes) or ambiguous.

## Conclusions

We should redo all analyses using the set of all (3440) abnormal clones to define the
denominator. As a start in this diection, we have recomputed the frequencies of the
LGF-abnormalities and saved them in a file called `abclone.rda`.

# Details
Here we supply the detailed computations behind the results described in the executive summary.

## Getting All the Data
We set up the paths and load an R package:
```{r paths}
source("00-paths.R")
library(RCytoGPS)
```
Load the original karyotype data.
```{r origkary}
load(file.path(paths$scratch, "blinded.rda"))
```
Load the LGF binary-transformed versions of the karyotypes.
```{r lgfmodel}
load(file.path(paths$raw, "rawLGF.Rda"))
```
Load the frequency summary statistics computed automatically by CytoGPS.
```{r freq}
load(file.path(paths$clean, "wicell11.Rda"))
```
See what we have so far.
```{r looksee}
ls()
```

## Merging Annotations
In order to determine the numbers of ESC and IPSC clones, we have to link the raw LGF data
back to the original "blinded" Excel spreadsheet.
```{r annotations}
annotations <- merge(WiCell.Info[, c(1,3)], blinded[, -6], 
               by.x = "Blinded.ID", by.y = "Blinded.ID.")
N <- c("Normal", "Abnormal")
annotations$State <- factor(N[1 + 1*(apply(rawLGF, 1, sum) > 0)], levels = N)
rownames(annotations) <- rownames(WiCell.Info)
rm(WiCell.Info)
```

## Basic Counting
We start counting at the level of the "raw" karyotypes sent from WiCell.

### All cultures
The total number of cultures sent to us from WiCell is obtained from the `blinded` data:
```{r allCultures}
head(blinded[,1:5])
nrow(blinded)
```

We can break this number down by the type of stem cell:
```{r allCulturesByType}
table(blinded$Cell.Type)
```

### Number of normal and abnormal cultures
Computing this split is going to take a little work, since it requires understanding
how the data were handled.

Zach Abrams manually (well, with a script) separated the culture-karyotypes
into "completely normal" (essentially, <tt>46,XX[20]</tt> or <tt>46,XY[20]</tt>) and
"at least partly abnormal". The intermediate results of that splitting process are not
immediately available. However, the abnormal culture karyotypes were used as input to
the CytoGPS algorithm at our web site (http://cytogps.org).

The output from CytoGPS is contained in the paired objects `rawLGF` and `annotations`.
A **key point** is that CytoGPS splits a karyotype into one or more clones, and thus
the size of this pair of objects counts "*clones contained in culture-karyotypes that
are at least partly abnormal*". To illustrate what we mean by this, consider the
beginning of the `annotations` object:
```{r anno}
head(annotations)
```
Here we see that the first two rows come from the same culture, with blinded id = 3.
The full "G-banded karyotype" (`r annotations[1,6]`) has been split into two different
clones, `r annotations[1,2]` and `r annotations[2,2]`. A second **key point** is that
all frequency computations we have used to date have used this number of *clones* as
the denominator, which is neither the number of cultures nor the number of abnormal
clones.

Now, in order to count the number of (partly) abnormal culture-karyotypes, we need to
determine the number of unique blinded ids contained in the annotations.
```{r uniq}
attach(annotations)
uniq <- c("Unique", "Extra")[1 + 1*duplicated(Blinded.ID)]
tab <- table(uniq, Cell.Type)
detach()
```
So, the total number of unique IDs here is the number of abnormal cultures:
```{r abcul}
nAbCulture <- sum(uniq == "Unique")
nAbCulture
```
and the corresponding number of normal cultures is
```{r nmcul}
nNmCulture <- nrow(blinded) - nAbCulture
nNmCulture
```
We can break the numbers of normal and abnormal cultures down by type of stem cell:
```{r breakdown}
U <- tab["Unique",]
T <- table(blinded$Cell.Type)
mat <- matrix(c(T - U, U), 2, 2, byrow = TRUE)
dimnames(mat) <- list(N, names(U))
mat
```
We can thus compute the rate at which we observe abnormal clones within ESC or
IPSC cultures.
```{r}
# fraction
round(100*U/T, 1)
rm(U, T)
```
And we can use a chi-squared test to see if these are different.
```{r chi-ab-cult}
chisq.test(mat)
```

### Number of clones from abnormal culture-karyotypes
To count the number of clones, we only need to use the output from CytoGPS.
```{r}
nrow(annotations)
```
We can easily break these down to compute the number of clones arising from
the two kinds of stem cells.
```{r}
sizes <- summary(annotations$Cell.Type)
sizes
```
We can further break this down into normal and abnormal clones.
```{r}
attach(annotations)
table(State)
stab <- table(State, Cell.Type)
detach()
stab
```
We can compute the rates of abnormalities:
```{r}
100*stab[2,]/apply(stab, 2, sum)
```

We can also perform a chi-squared test here.
```{r}
chisq.test(stab)
```

### Abnormal Clones
We want to look more carefully at the set of (3440) abnormal clones.
```{r isac}
isAbnormalClone <- annotations$State == "Abnormal"
summary(isAbnormalClone)
abCloneLGF <- rawLGF[isAbnormalClone,]
abCloneAnno <- annotations[isAbnormalClone,]
```
We confirm the counts of abnormal clones, by cell type.
```{r}
nrow(abCloneAnno) # total
summary(abCloneAnno$Cell.Type)
summary(abCloneAnno$Male)
table(abCloneAnno$Cell.Type, abCloneAnno$Male)
```
Now we count the unique cultures in this set.
```{r uniq2}
attach(abCloneAnno)
uniq2 <- c("Unique", "Extra")[1 + 1*duplicated(Blinded.ID)]
tab2 <- table(uniq2, Cell.Type)
detach()
```
So, the total number of unique IDs here is the number of abnormal cultures:
```{r weird}
table(uniq2)
```

AARRRGGGHH!!! Why doesn't this number match the stuff above????
```{r}
u1bid <- annotations$Blinded.ID[uniq == "Unique"]
u2bid <- abCloneAnno$Blinded.ID[uniq2 == "Unique"]
all(u2bid %in% u1bid)
weird <- u1bid[!(u1bid %in% u2bid)]
X <- blinded[blinded$Blinded.ID. %in% weird,]
write.csv(X[, c(1,5)], file=file.path(paths$scratch, "weird.csv"), row.names = FALSE)
```
We looked at the exported file; for various reasons, these are all karyotypes that
CytoGPS recognizes as "normal" even though they may not be. (Some examples: <tt>45,X</tt>;
<tt>47,XXY</tt>; or <tt>46,XY[12]/46,XX[8]</tt>.) We believe this result to reflect a bug
in CytoGPS that will eventually be fixed.

### Cluster Counts, Revisited
So, we are going to *recount the abnormal clusters* after throwing these odd things away.
```{r}
bilk <- blinded[blinded$Blinded.ID. %in% u2bid,]
dim(bilk)
head(bilk)
s <- summary(bilk$Cell.Type)
```
First, we split up the number of abnormal clusters by type of stem cell:
```{r}
s
```
Next, we reconstruct the $2\times2$ matrix, keeping the same initial number and
distribution of normal cultures.
```{r}
countByCulture <- rbind(mat["Normal",], matrix(s, nrow=1))
rownames(countByCulture) <- N
countByCulture
```
Next, we reconstruct the total number of cultures that we (should have) started with:
```{r}
apply(countByCulture, 2, sum)
sum(countByCulture)
```
And we compute the percentages of abnormal cultures by cell type.
```{r}
100*countByCulture["Abnormal", ]/apply(countByCulture, 2, sum)
```
Finally, we reapply a chi-squared test to see if abnormalities occur more frequently
in one type of stem cell.
```{r}
chisq.test(countByCulture)
```

```{r}
A <- table(annotations$State, annotations$Cell.Type, 
           hasAb = annotations$Blinded.ID %in% u2bid)
A
```

```{r}
countByClone <- A[,,2]
sum(countByClone)
apply(countByClone, 1, sum)
apply(countByClone, 2, sum)
100*countByClone[1,]/apply(countByClone, 2, sum)
chisq.test(countByClone)
```

```{r saveme}
save(countByClone, countByCulture, file = file.path(paths$results, "counters.rda"))
```

## Final Counts
We use this function to summarize the counts.
```{r ref.label="summarizeCounts", eval=FALSE}
```

### Cultures
```{r}
summarizeCounts(countByCulture)
```

### Clones
```{r}
summarizeCounts(countByClone)
```

# Clonal Evolution or Clonal Divergence
One of the major questions we want to (try to) address is whether multiple abnormal clones
arising in a single culture share an abnormality (and so could be explained as clonal evolution)
or contain totally distinct abniormalities (reflecting clonal divergence).

Here, for each possible number of clones, we count the number of cultures containing that
number of abnormal clones.
```{r}
table(bit <- table(abCloneAnno$Blinded.ID))
length(bit) # number of cultures
```
Of the 3021 cultures containing a total of 3440 clones, there are 2673 cultures that contain
exactly one abnormal clone, and `r 3021-2673` cultures that contain multiple distinct abnormal
clones.

## Jaccard Distance Between Multiple Abnormal Clones
For each culture containing more than one abnormal clone, we will compute the Jaccard
distance between (the LGF-binary rerpesentation of) those clones.
```{r mjd}
suppressMessages( library(Mercator) )
multi <- names(bit[bit > 1])
f <- file.path(paths$scratch, "mjd.rda")
if (file.exists(f)) {
  load(f)
} else {
  mjd <- sapply(multi, function(M) {
    w <- abCloneAnno$Blinded.ID == M
    picked <- abCloneLGF[w,]
    binaryDistance(t(picked), "jaccard")
  })
  save(mjd, file = f)
}
rm(f)
mjdx <- sapply(mjd, max)
mjdn <- sapply(mjd, min)
mjdi <- sapply(mjd, function(x) sum(x == 1))
ct <- blinded[multi, "Cell.Type"]
```

If the maximum distance between clones is 0, then all those clones have identical 
LGF representations. If the maximum distance is 1, then at least two clones are
strongly independent. sharing no abnormaliites in common.
```{r}
prop <- rep("Shared", length(mjd))
prop[mjdx == 0] <- "Identical"
prop[mjdx == 1] <- "Distinct"
prop <- factor(prop, levels = c("Identical", "Shared", "Distinct"))
ptab <- table(prop, ct)
ptab
chisq.test(ptab)
```
There appears to be no difference between ESC and IPSC stem cells with regard to the
distribution of independent versus distinct clones, 
```{r beans, fig.cap = "Jaccard distance between clones by stem cell type."}
library(beanplot)
beanplot(mjdx ~ ct, what = c(1,1,1,0))
```

Here is a detailed look at the Jaccard distance between clones in cases where a single
culture has at least 3 different abnormal clones.
```{r}
mjd[mjdi==3]
```
For the majority of those examples, all the clones are strongly independent. 
Here are even more details abiuyt the three cases where independence is not
so clear.
```{r}
mjd[["12664"]]
annotations[annotations$Blinded.ID == 12664, 1:4]
```
In case `12664`, the overlap comes from different fusions that involve chromosome 1,
so these look independent.

```{r}
mjd[["8046"]]
annotations[annotations$Blinded.ID == 8046, 1:4]
```
In case `8046`, again the overlap comes from different fusions,

```{r}
mjd[["4571"]]
annotations[annotations$Blinded.ID == 4571,1:4]
```
Case `4571` is more interesting. The clone `47,XY,+i(X)(p10)` is clearly independent of
everything else. It seems likely that
the clone `48,XY,+8,+12` evolved from the clone `47,XY,+12`. And while it is possible
that the `47,XY,+i(12)(p10)` evolved from the same base, we can't tell (either from
the LGF model or from the ISCN karyotype).

```{r echo=FALSE, eval=FALSE}
paird <- function(mat) {
  mat <- as.matrix(mat)
  N <- nrow(mat)
  res <- matrix(rep(NA, N^2), N, N)
  dimnames(res) <- list(row.names(mat), row.names(mat))
  for(I in 1:N) {
    temp <- sweep(mat, 2, mat[I,], "-")
    res[I,] <- apply(temp, 1, function(x)sum(x > 0))
  }
  res
}
f <- file.path(paths$scratch, "walloo.rda")
if (file.exists(f)) {
  load(f)
} else {
  walloo <- sapply(multi, function(M) {
    w <- abCloneAnno$Blinded.ID == M
    picked <- abCloneLGF[w,]
    paird(picked)
  })
  save(walloo, file = f)
}
rm(f)
walloo[mjdi == 3]
evolved <- sapply(walloo, function(x) {sum(x==0) - sum(diag(x)==0)} )
evolved["4571"]

K1 <- abCloneLGF["WC4571.K1",]
K2 <- abCloneLGF["WC4571.K2",]
LGF <- rep(c("L","G", "F"), times = ncol(abCloneLGF)/3)
table(K1, K2, LGF)
```

# Frequencies Redux
Now we recompute the frequencies based on the number of abnormal clones.
```{r}
extract <- function(X) {
  mx <- 100*apply(X, 2, mean)
  M <- matrix(mx, ncol=3, byrow = TRUE)
  colnames(M) <- c("Loss", "Gain", "Fusion")
  foo  <- sub("_Loss", "", names(mx)[seq(1, length(mx), 3)])
  g <- grep("^X\\d+", foo)
  foo[g] <- sub("^X", "", foo[g])
  foo <- sub("\\.$", "", foo)
  rownames(M) <- foo
  M
}
totfreq <- extract(abCloneLGF)
escfreq <- extract(abCloneLGF[abCloneAnno$Cell.Type == "ESC",])
ipscfreq <- extract(abCloneLGF[abCloneAnno$Cell.Type == "IPSC",])
Mfreq <- extract(abCloneLGF[abCloneAnno$Male,])
Ffreq <- extract(abCloneLGF[!abCloneAnno$Male,])
freq <- data.frame(IPSC = ipscfreq, ESC = escfreq, Merged = totfreq, Male = Mfreq, Female = Ffreq)
all(rownames(wicell@DATA) %in% rownames(freq))
freq <- freq[rownames(wicell@DATA),]
abCloneFreq <- data.frame(wicell@DATA[, 1:6], freq)
```

```{r}
save(abCloneAnno, abCloneLGF, abCloneFreq, file = file.path(paths$clean, "abclone.rda"))
```

# Appendix
These computations were performed in the following environment:
```{r si}
sessionInfo()
```
and in the following directory:
```{r where}
getwd()
```
