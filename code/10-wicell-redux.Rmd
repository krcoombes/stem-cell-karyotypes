---
author: "Kevin R. Coombes"
title:  "WiCell Two-sided Idiograms, **Redone**"
date:   "`r Sys.Date()`"
output:
  html_document:
    toc: true
    highlight: kate
    theme: readable
---

```{r setup, include=FALSE, results="hide"}
knitr::opts_chunk$set(echo = TRUE)
options(width = 80)
```
```{r mycss, results="asis", echo=FALSE}
cat('
<style type="text/css">
b, strong {color: red; }
i, em {color: blue; }
.defn {color: purple; }
.para {color: purple;
      font-weight: bold;
}
.figure { text-align: center; }
.caption { font-weight: bold; }
</style>
')
```

# Overview
The goal of this analysis is display the summary
statistics from our analysis of the WiCell karyotype data.
Our previous attempt to do this (in report `02-wicell-biIdiiograms`)
computed *separate* scales for each chromosome, which gioves a
potentially misleading global overview of the frequency of abnormalities.
Here we fix that error and compute a single common scale with which to
display all chromosomes on the same plot.

# Getting Started
First, we load the generic idiogram data produced in the previous report.
```{r idio}
source("00-paths.R")
library(RCytoGPS)
data("cytobandLocations")
```

# WiCell Data
Now we read the stem cell frequency results.
```{r wisc}
load(file.path(paths$clean, "allFreq.Rda"))
all(rownames(cytobandLocations) == rownames(allFreq))
all(rownames(cytobandLocations) %in% rownames(allFreq))
allFreq <- allFreq[rownames(cytobandLocations),]
all(rownames(cytobandLocations) == rownames(allFreq))
wisc <- data.frame(cytobandLocations, allFreq[, c(2, 4:9)])
rm(idiocolors, cytobandLocations, allFreq)
summary(wisc)
save(wisc, file = file.path(paths$clean, "wisc.Rda"))
```

# Idiograms
We define a palette to mark the tpes of stem cells
```{r, fig.cap="LGF palette"}
library(Polychrome)
data(colorsafe)
pal <- colorsafe[c(9, 8)]
names(pal) <- c("ESC", "IPSC")
swatch(pal)
```


## Bi-idiogram
Here we define a function to make an idiogram that shows different data on either side of each
chromosome.
```{r biIdiogram}
wicell <- CytobandData(wisc)
```

```{r}
res <- 300
png(file = file.path(paths$results, "wicell-losses-2.png"),
    width=18*res, height=10*res, res=res, bg="white")
image(wicell, chr="all", 
      what = list("ESC.Loss", "IPSC.Loss"),
      pal = pal, axes = FALSE, legend = TRUE)
dev.off()
```

![Losses by Chromosome](`r file.path(paths$results, "wicell-losses-2.png")`)

```{r}
png(file = file.path(paths$results, "wicell-gains-2.png"),
    width=18*res, height=10*res, res=res, bg="white")
image(wicell, chr="all", 
      what = list("ESC.Gain", "IPSC.Gain"),
      pal = pal, axes = FALSE, legend = TRUE)
dev.off()
```

![Gains by Chromosome](`r file.path(paths$results, "wicell-gains-2.png")`)

```{r}
png(file = file.path(paths$results, "wicell-fusions-2.png"),
    width=18*res, height=10*res, res=res, bg="white")
image(wicell, chr="all", 
      what = list("ESC.Fusion", "IPSC.Fusion"),
      pal = pal, axes = FALSE, legend = TRUE)
dev.off()
```

![Fusions by Chromosome](`r file.path(paths$results, "wicell-fusions-2.png")`)


# Appendix
These computations were performed in the following environment:
```{r si}
sessionInfo()
```
and in the following directory:
```{r where}
getwd()
```
