# DATA must have three columns, one of shich is "Chromosome" containing
# entries of the form "chr1".
biIdiogram  <- function(DATA, leftcol, rightcol, 
                        pal = c("blue", "red"), 
                        horizontal=FALSE, nrows = 2) {
  if(!nrows %in% 1:4) {
    stop("Number of rows must be 1, 2, 3, or 4.")
  }
  opar <- par(bg="white")
  on.exit(par(opar))
  # vertical layout, two rows
  L1 <- function() {
    layout(matrix(1:72, 1, 72, byrow = TRUE),
           heights = c(1), widths=rep(c(1.1, 0.8, 1.1), times = 24))
  }
  L2 <- function() {
    layout(matrix(1:72, 2, 36, byrow = TRUE),
           heights = c(1,1), widths=rep(c(1.1, 0.8, 1.1), times = 12))
  }
  L3 <- function() {
    layout(matrix(1:72, 3, 24, byrow = TRUE),
           heights = c(1,1, 1), widths=rep(c(1.1, 0.8, 1.1), times = 8))
  }
  L4 <- function() {
    layout(matrix(1:72, 4, 18, byrow = TRUE),
           heights = c(1,1, 1, 1), widths=rep(c(1.1, 0.8, 1.1), times = 6))
  }
  switch(nrows, L1(), L2(), L3(), L4())

  for (I in c(1:22, "X", "Y")) { # for each chromosome
    chrname <- paste('chr', I, sep='')
    dumbposn <- seq(1, 250000000, length=2500)
    clap <- idio[idio$Chromosome == chrname,]
    segset <- DATA[DATA$Chromosome == chrname,]
    leftvals <- rightvals <- y <- rep(NA, length(dumbposn))
    for(J in 1:nrow(clap)) {
      y[clap[J, "loc.start"] <= dumbposn & 
          dumbposn <= clap[J, "loc.end"] ] <- as.numeric(clap[J, "Stain"])
      rightvals[clap[J, "loc.start"] <= dumbposn & 
          dumbposn <= clap[J, "loc.end"] ] <- as.numeric(segset[J, rightcol])
      leftvals[clap[J, "loc.start"] <= dumbposn & 
          dumbposn <= clap[J, "loc.end"] ] <- as.numeric(segset[J, leftcol])
    }
    resn <- max(max(segset[,leftcol]), max(segset[,rightcol]))
    ## left bars
    par(mai=c(0, 0.001, 0.5, 0.001))
    barplot(-rev(leftvals), horiz=T, border=NA, col=pal[1],
            xlim=c(-1.05*resn, 0), yaxs="i", xaxt="n", space=0)
    ## chromosomes
    par(mai=c(0, 0.02, 0.5, 0.02))
    image(1:1, dumbposn, matrix(rev(y), nrow=1), col=idiocolors, bty='n',
          xlab='', ylab='', xaxt='n', yaxt='n', zlim=c(1, 8),
          main=paste("Chr", I), cex=0.8)
    pts <- max(dumbposn) - c(min(clap$loc.start), max(clap$loc.end))
    abline(h=pts)
    lines(c(1.4, 1.4), pts)
    lines(c(0.6, 0.6), pts)
    ## right bars
    par(mai=c(0, 0.001, 0.5, 0.001))
    barplot(rev(rightvals), horiz=T, border=NA, col=pal[2],
            xlim=c(0, 1.05*resn), yaxs="i", xaxt="n", space=0)
  }
  layout(1,1,1)
  par(new = TRUE)
  legend(0.3*par("usr")[2], 0.15*par("usr")[4], c(leftcol, rightcol), col=pal, pch=15)
#  box()
  invisible(DATA)
}
