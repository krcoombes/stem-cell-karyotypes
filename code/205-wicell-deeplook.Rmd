---
title: 'WiCell 205: Burrowing into Karyotypes'
author: "Kevin R. Coombes"
date: "`r Sys.Date()`"
output:
  html_document:
    highlight: kate
    theme: readable
    toc: yes
  pdf_document:
    toc: yes
---

```{r setup, include=FALSE, results="hide"}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_chunk$set(fig.path = "../results/figs/")
options(width = 80)
```
```{r mycss, results="asis", echo=FALSE}
cat('
<style type="text/css">
b, strong {color: red; }
i, em {color: blue; }
.defn {color: purple; }
.para {color: purple;
      font-weight: bold;
}
.figure { text-align: center; }
.caption { font-weight: bold; }
</style>
')
```

# Overview
The goal of this analysis is to determine which abnormalities occur at significant rates
overall in stem cells after pooling the ESC and ISPC karyotypes.

# Getting All the Data
We set up the paths and lod an important R pacakge
```{r paths}
source("00-paths.R")
library(RCytoGPS)
load(file.path(paths$clean, "abclone.rda"))
load(file.path(paths$clean, "Wicell.Rda"))
ls()
```

Here is a summary of the core data frame.
```{r}
summary(wicell@DATA)
```
Notice that the frequencies for the combined data are inth e three columns labeled "Merge".

# Visualizations
```{r lib}
library(Polychrome)
data("colorsafe")
pal <- as.list(colorsafe[c(7, 3, 1)])
names(pal) <- c("L", "G", "F")
```

```{r MergedSCLoss, fig.width=15.3, fig.height=12, fig.cap="Genome-wide plots of combined stem cell events."}
image(wicell, chr = "all", what = "Merged.Loss", pal= pal$L, legend = TRUE)
```

```{r MergedSCGain, fig.width=15.3, fig.height=12, fig.cap="Genome-wide plots of combined stem cell events."}
image(wicell, chr = "all", what = "Merged.Gain", pal= pal$G, legend = TRUE)
```

```{r MergedSCFusion, fig.width=15.3, fig.height=12, fig.cap="Genome-wide plots of combined stem cell events."}
image(wicell, chr = "all", what = "Merged.Fusion", pal= pal$F, legend = TRUE)
```

# Aggregation

```{r aggro}
X <- wicell@DATA[, c(1, 6, grep("Merged", colnames(wicell@DATA)))]
aggro <- aggregate(. ~ Chromosome + Arm, X, max, na.rm=TRUE)
summary(aggro)
```

```{r junk, fig.cap="Sorted p-values."}
M <- as.matrix(aggro[, 3:5])
plot(sort(M))
plot(sort(M)[110:144])
```

# Tabulation

```{r}
foo <- function(x) {
  men <- sprintf("%.2f", round(mean(x), 2))
  rng <- paste(" (",
               sprintf("%.2f", round(min(x), 2)), ", ",
               sprintf("%.2f", round(max(x), 2)), ")", sep = "")
  list(mean = men, range = rng)
}
describe <- function(type, lgf, seg, daft) {
  x <- daft[seg, paste(type, lgf, sep = ".")]
  foo(x)
}
delta <- function(lgf, seg, daft) {
  x <- daft[seg, paste("ESC", lgf, sep = ".")]
  y <- daft[seg, paste("IPSC", lgf, sep = ".")]  
  foo(x - y)
}
```

```{r f0.5}
output <- data.frame(CHromosome=NA,
                     Region = NA, Type = NA, Frequency = NA, Range = NA,
                     ESC = NA, ESCRange = NA, IPSC = NA, IPSCRange = NA)[-1,]
dset <- wicell@DATA
sizes <- summary(abCloneAnno$Cell.Type)
for (CH in c(1:22, "X", "Y")) {
  for (arm in c("p", "q")) {
    CHR <- paste("chr", CH, sep ="")
    pick <- (dset$Chromosome == paste("chr", CH, sep ="") & dset$Arm == arm)
    sst <- dset[pick,]
    for (lgf in c("Loss", "Gain", "Fusion")) {
      pname <- paste("Merged", lgf, sep=".")
      pvname <- paste("pval", lgf, sep=".")
#      where <- which(sst[, pname] > 0.5)
      where <- which(sst[, pname] > 0.5 | sst[, pvname] < 0.05)
      if (length(where) == 0) {
        next
      }
      temp <- c(0, which(diff(where) > 1), length(where))
      begin <- 1 + temp[1:(length(temp) - 1)]
      ending <- temp[2:length(temp)]
      for (I in 1:length(begin)) {
        R <- rownames(sst)[where]
        loci <- ifelse(arm == "p",
                       paste(R[ending[I]], R[begin[I]], sep="-"),
                       paste(R[begin[I]], R[ending[I]], sep="-"))
        SC <- describe("Merged", lgf, begin[I]:ending[I], sst[where,])
        esc <- describe("ESC", lgf, begin[I]:ending[I], sst[where,])
        ipsc <- describe("IPSC", lgf, begin[I]:ending[I], sst[where,])
        del <- delta(lgf, begin[I]:ending[I], sst[where,])
        pv <- min(sst[where, pvname], na.rm = TRUE)
        dframe <- data.frame(Chromosome=CHR,
                             Region = loci,
                             Type = lgf,
                             MeanFrequency = SC$mean,
                             Range = SC$range,
                             ESC = esc$mean,
                             ESCRange = esc$range,
                             IPSC = ipsc$mean,
                             IPSCRange = ipsc$range)
        output <- rbind(output, dframe)
      }
    }
  }
}
output$SortIndex = 1:nrow(output)
write.csv(output, file = file.path(paths$results, "freqTable-0-5-NEW.csv"), row.names = FALSE)
```

```{r combo}
diffs <- read.csv(file.path(paths$results, "differenceTable.csv"), header=TRUE)
diffs$SortIndex <- 1:nrow(diffs)
combo <- merge(output, diffs, by=c("Region", "Type"), all=TRUE)
for (cn in c("Chromosome",  "ESC", "ESCRange", "IPSC", "IPSCRange")) {
  xcol <- combo[, xn <- paste(cn, "x", sep=".")]
  ycol <- combo[, paste(cn, "y", sep=".")]
  combo[is.na(xcol), xn] <- ycol[is.na(xcol)]
}
gone <- grep("\\.y", colnames(combo))[1:5]
combo <- combo[, -gone]
ren <- grep("\\.x$", colnames(combo))[1:5]
colnames(combo)[ren] <- sub("\\.x$", "", colnames(combo)[ren])
combo <- combo[, c(3, 1, 2, 4:9, 11:13)]
```

```{r order4COmbo}
combo$Chromosome <- factor(combo$Chromosome, 
                           levels = paste("chr", c(1:22, "X", "Y"), sep = ""))
edges <- strsplit(combo$Region, "-")
starter <- sapply(edges, function(x) x[[1]])
ender <- sapply(edges, function(x) x[[2]])
ends <- c(starter, ender)
punct <- regexpr("[pq]", ends)
pop <- sapply(ends, nchar)
sop <- sort(unique(bogo <- substr(ends, punct, pop)))
lvls <- c(rev(sop[1:30]), sop[31:85])
starter <- factor(bogo[1:118], levels = lvls)
ender <- factor(bogo[119:236], levels = lvls)
combo <- combo[order(combo$Chromosome, starter), ]
combo$SortIndex <- 1:nrow(combo)

write.csv(combo, file = file.path(paths$results, "combinationTable.csv"), row.names = FALSE)
```

# Cases With Specific Abnormalities

## Chr 1
```{r chr1}
loci <-c(grep("X1p",colnames(abCloneLGF)), grep("X1q",colnames(abCloneLGF)))
tick <- apply(abCloneLGF[, loci], 1, sum)
sum(tick > 0)
chr1stuff <- abCloneAnno[tick > 0,]
write.csv(chr1stuff, file = file.path(paths$scratch, "chr1-kary.csv"))
```

## Chr 2
```{r chr2}
loci <-c(grep("X2q24",colnames(abCloneLGF)),
         grep("X2q32",colnames(abCloneLGF)))
tick <- apply(abCloneLGF[, loci], 1, sum)
sum(tick > 0)
chr2stuff <- abCloneAnno[tick > 0,]
write.csv(chr2stuff, file = file.path(paths$scratch, "chr2-kary.csv"))
```

## Chr 3
```{r chr3}
loci <-c(grep("X3p25",colnames(abCloneLGF)))
tick <- apply(abCloneLGF[, loci], 1, sum)
sum(tick > 0)
chr3stuff <- abCloneAnno[tick > 0,]
write.csv(chr3stuff, file = file.path(paths$scratch, "chr3-kary.csv"))
```

## Chr 6
```{r chr6}
loci <-c(grep("X6p23",colnames(abCloneLGF)), grep("X6p24",colnames(abCloneLGF)))
tick <- apply(abCloneLGF[, loci], 1, sum)
sum(tick > 0)
chr6stuff <- abCloneAnno[tick > 0,]
write.csv(chr6stuff, file = file.path(paths$scratch, "chr6-kary.csv"))
```

## Chr 7
```{r chr7}
loci <-c(grep("X7q11",colnames(abCloneLGF)), grep("X7q21",colnames(abCloneLGF)))
loci <-c(grep("X7p",colnames(abCloneLGF)), grep("X7q",colnames(abCloneLGF)))
tick <- apply(abCloneLGF[, loci], 1, sum)
sum(tick > 0)
chr7stuff <- abCloneAnno[tick > 0,]
write.csv(chr7stuff, file = file.path(paths$scratch, "chr7-kary.csv"))
```



# Appendix
These computations were performed in the following environment:
```{r si}
sessionInfo()
```
and in the following directory:
```{r where}
getwd()
```
